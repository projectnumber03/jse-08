package ru.shilov.tm;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.other.AboutCommand;
import ru.shilov.tm.command.other.ExitCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.command.project.*;
import ru.shilov.tm.command.task.*;
import ru.shilov.tm.command.user.*;
import ru.shilov.tm.context.Bootstrap;

public final class Application {

    @SuppressWarnings("unchecked")
    public static void main(@NotNull final String[] args) throws Exception {
        @NotNull final Class[] classes = {
                TaskFindAllCommand.class
                , ProjectFindAllCommand.class
                , ProjectFindOneCommand.class
                , TaskMergeCommand.class
                , ProjectMergeCommand.class
                , TaskPersistCommand.class
                , ProjectPersistCommand.class
                , TaskRemoveAllCommand.class
                , ProjectRemoveAllCommand.class
                , TaskRemoveCommand.class
                , ProjectRemoveCommand.class
                , TaskSelectCommand.class
                , ProjectSelectCommand.class
                , TaskAttachCommand.class
                , ExitCommand.class
                , UserFindAllCommand.class
                , UserLoginCommand.class
                , UserLogoutCommand.class
                , UserMergeCommand.class
                , UserPasswordChangeCommand.class
                , UserRegisterCommand.class
                , UserSelectCommand.class
                , ProjectClearCommand.class
                , TaskClearCommand.class
                , AboutCommand.class
                , HelpCommand.class
        };
        new Bootstrap().init(classes);
    }

}
