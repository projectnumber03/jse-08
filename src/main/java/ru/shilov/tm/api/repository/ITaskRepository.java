package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.Task;

public interface ITaskRepository extends IRepository<Task> {

    @NotNull String getId(@NotNull final String value, @NotNull final String userId);

}
