package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IRepository<T> {

    @NotNull List<T> findAll();

    @Nullable T findOne(@NotNull final String id);

    @NotNull List<T> findByUserId(@NotNull final String userId);

    void removeAll();

    @NotNull Boolean removeByUserId(@NotNull final String userId);

    @NotNull Boolean removeOneByUserId(@NotNull final String id, @NotNull final String userId);

    @Nullable T persist(@NotNull final T entity);

    @NotNull T merge(@NotNull final T entity);

}
