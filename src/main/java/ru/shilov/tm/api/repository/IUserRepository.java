package ru.shilov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.User;

import java.util.Optional;

public interface IUserRepository extends IRepository<User> {

    @NotNull String getId(@NotNull final String value);

    @NotNull Boolean removeOneByUserId(@NotNull final String id, @NotNull final String userId);

    @NotNull Optional<User> findByLoginPassword(@NotNull final String login, @NotNull final String passwd);

    @NotNull Boolean containsLogin(@NotNull final String login);

}
