package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException;

    @NotNull Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException;

    @NotNull String getId(@Nullable final String value, @Nullable final String userId) throws NumberToIdTransformException;

}
