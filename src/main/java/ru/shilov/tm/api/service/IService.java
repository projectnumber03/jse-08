package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.error.EntityMergeException;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.EntityRemoveException;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.List;

public interface IService<T> {

    @NotNull List<T> findAll();

    @NotNull T findOne(@Nullable final String id) throws NoSuchEntityException;

    void removeAll();

    @NotNull Boolean removeOneByUserId(@Nullable final String id, @Nullable final String userId) throws EntityRemoveException;

    @Nullable T persist(@Nullable final T entity) throws EntityPersistException;

    @Nullable T merge(@Nullable final T entity) throws EntityMergeException;

}
