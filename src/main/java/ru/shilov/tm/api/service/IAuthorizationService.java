package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public interface IAuthorizationService {

    @NotNull String getCurrentUserId();

    void setCurrentUser(@NotNull final Optional<User> currentUser);

    @NotNull Boolean hasAnyRole(@NotNull final List<User.Role> roles);

}
