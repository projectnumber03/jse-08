package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;
import ru.shilov.tm.error.NumberToIdTransformException;

import java.util.Optional;

public interface IUserService extends IService<User> {

    @NotNull String getId(@Nullable final String value) throws NumberToIdTransformException;

    @NotNull Boolean containsLogin(@Nullable final String login) throws NoSuchEntityException;

    @NotNull Optional<User> findByLoginPassword(@Nullable final String login, @Nullable final String passwd) throws NoSuchEntityException;

}
