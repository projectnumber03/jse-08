package ru.shilov.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITerminalService {

    @NotNull String nextLine();

}
