package ru.shilov.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.*;

public interface ServiceLocator {

    @NotNull IProjectService getProjectService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

    @NotNull ITerminalService getTerminalService();

    @NotNull IAuthorizationService getAuthorizationService();

}
