package ru.shilov.tm.context;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.api.service.*;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.command.other.HelpCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityPersistException;
import ru.shilov.tm.error.IllegalCommandException;
import ru.shilov.tm.error.InitializationException;
import ru.shilov.tm.error.PermissionException;
import ru.shilov.tm.repository.ProjectRepositoryImpl;
import ru.shilov.tm.repository.TaskRepositoryImpl;
import ru.shilov.tm.repository.UserRepositoryImpl;
import ru.shilov.tm.service.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public final class Bootstrap implements ServiceLocator {

    private @NotNull final IProjectRepository projectRepo = new ProjectRepositoryImpl();

    private @NotNull final ITaskRepository taskRepo = new TaskRepositoryImpl();

    private @NotNull final IUserRepository userRepo = new UserRepositoryImpl();

    @Getter
    private @NotNull final ITerminalService terminalService = new TerminalServiceImpl();

    @Getter
    private @NotNull final IAuthorizationService authorizationService = new AuthorizationServiceImpl();

    @Getter
    private @NotNull final IProjectService projectService = new ProjectServiceImpl(projectRepo, taskRepo);

    @Getter
    private @NotNull final ITaskService taskService = new TaskServiceImpl(taskRepo);

    @Getter
    private @NotNull final IUserService userService = new UserServiceImpl(userRepo);

    public void init(@NotNull final Class<AbstractTerminalCommand>[] classes) throws Exception {
        @NotNull final Map<String, AbstractTerminalCommand> commands = initCommands(classes);
        System.out.println("*** ДОБРО ПОЖАЛОВАТЬ В ПЛАНИРОВЩИК ЗАДАЧ ***");
        initUsers();
        while (true) {
            try {
                final String commandName = terminalService.nextLine();
                if (!commands.containsKey(commandName)) throw new IllegalCommandException();
                final AbstractTerminalCommand command = commands.get(commandName);
                if (!command.getRoles().isEmpty() && !authorizationService.hasAnyRole(command.getRoles())) {
                    throw new PermissionException();
                }
                command.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private Map<String, AbstractTerminalCommand> initCommands(@NotNull final Class<AbstractTerminalCommand>[] classes) {
        @NotNull final Map<String, AbstractTerminalCommand> commands = new HashMap<>();
        commands.putAll(Arrays.stream(classes).map(c -> {
            try{
                if (AbstractTerminalCommand.class.isAssignableFrom(c)) {
                    AbstractTerminalCommand tc = c.getDeclaredConstructor().newInstance();
                    tc.setServiceLocator(this);
                    if (tc instanceof HelpCommand) ((HelpCommand) tc).setCommands(commands);
                    return tc;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).filter(Objects::nonNull).collect(Collectors.toMap(AbstractTerminalCommand::getName, tc -> tc)));
        return commands;
    }

    private void initUsers() throws Exception {
        try {
            userService.persist(new User("user", "123", User.Role.USER));
            userService.persist(new User("admin", "123", User.Role.ADMIN));
        } catch (EntityPersistException e) {
            throw new InitializationException();
        }
    }

}
