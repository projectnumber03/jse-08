package ru.shilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.ITaskService;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.ITaskRepository;

import java.util.List;

@AllArgsConstructor
public final class TaskServiceImpl extends AbstractService<Task> implements ITaskService {

    @Getter
    private @NotNull final ITaskRepository repository;

    @Override
    public @NotNull List<Task> findByUserId(@Nullable final String userId) throws NoSuchEntityException {
        if (userId == null || userId.isEmpty()) throw new NoSuchEntityException();
        return repository.findByUserId(userId);
    }

    @Override
    public @NotNull Boolean removeByUserId(@Nullable final String userId) throws EntityRemoveException {
        if (userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeByUserId(userId);
    }

    @Override
    public @NotNull Boolean removeOneByUserId(@Nullable final String id, @Nullable final String userId) throws EntityRemoveException {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @Override
    public @NotNull String getId(@Nullable final String value, @Nullable final String userId) throws NumberToIdTransformException {
        if (!isNumber(value) || userId == null || userId.isEmpty()) throw new NumberToIdTransformException(value);
        return repository.getId(value, userId);
    }

    private boolean isNumber(@Nullable final String value) {
        return value != null && !value.isEmpty() && value.matches("\\d+");
    }

}
