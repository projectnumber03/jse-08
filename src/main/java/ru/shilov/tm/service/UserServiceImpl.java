package ru.shilov.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.service.IUserService;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.*;
import ru.shilov.tm.api.repository.IUserRepository;

import java.util.Optional;

@AllArgsConstructor
public final class UserServiceImpl extends AbstractService<User> implements IUserService {

    @Getter
    private final IUserRepository repository;

    @Override
    public @NotNull Boolean removeOneByUserId(@Nullable final String id, @Nullable final String userId) throws EntityRemoveException {
        if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) throw new EntityRemoveException();
        return repository.removeOneByUserId(id, userId);
    }

    @Override
    public User persist(@Nullable final User user) throws EntityPersistException {
        if (user == null) throw new EntityPersistException();
        user.setPassword(DigestUtils.md5Hex(user.getPassword()));
        return repository.persist(user);
    }

    @Override
    public @NotNull String getId(@Nullable final String value) throws NumberToIdTransformException {
        if (!isNumber(value)) throw new NumberToIdTransformException(value);
        return repository.getId(value);
    }

    @Override
    public @NotNull Boolean containsLogin(@Nullable final String login) throws NoSuchEntityException {
        if (login == null || login.isEmpty()) throw new NoSuchEntityException();
        return repository.containsLogin(login);
    }

    @Override
    public @NotNull Optional<User> findByLoginPassword(@Nullable final String login, @Nullable final String passwd) throws NoSuchEntityException {
        if (login == null || login.isEmpty() || passwd == null || passwd.isEmpty()) throw new NoSuchEntityException();
        return repository.findByLoginPassword(login, passwd);
    }

    private boolean isNumber(@Nullable final String value) {
        return value != null && !value.isEmpty() && value.matches("\\d+");
    }

}
