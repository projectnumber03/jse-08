package ru.shilov.tm.service;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.IAuthorizationService;
import ru.shilov.tm.entity.AbstractEntity;
import ru.shilov.tm.entity.User;

import java.util.List;
import java.util.Optional;

public final class AuthorizationServiceImpl implements IAuthorizationService {

    @Setter
    private @NotNull Optional<User> currentUser = Optional.empty();

    @Override
    public @NotNull String getCurrentUserId() {
        return currentUser.map(AbstractEntity::getId).orElse("");
    }

    @Override
    public @NotNull Boolean hasAnyRole(@NotNull final List<User.Role> roles) {
        return currentUser.isPresent() && roles.contains(currentUser.get().getRole());
    }

}
