package ru.shilov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.service.ITerminalService;

import java.util.Scanner;

public final class TerminalServiceImpl implements ITerminalService {

    private @NotNull final Scanner scanner = new Scanner(System.in);

    @Override
    public @NotNull String nextLine() {
        return scanner.nextLine();
    }

}
