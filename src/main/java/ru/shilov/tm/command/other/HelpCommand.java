package ru.shilov.tm.command.other;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class HelpCommand extends AbstractTerminalCommand {

    @Setter
    private Map<String, AbstractTerminalCommand> commands;

    @Override
    public @NotNull String getName() {
        return "help";
    }

    @Override
    public @NotNull String getDescription() {
        return "Список доступных комманд";
    }

    @Override
    public void execute() {
        @NotNull final String helpMessage = commands.values().stream()
                .sorted(Comparator.comparing(AbstractTerminalCommand::getName))
                .map(tc -> String.format("%s: %s", tc.getName(), tc.getDescription()))
                .collect(Collectors.joining("\n"));
        System.out.println(helpMessage);
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Collections.emptyList();
    }

}
