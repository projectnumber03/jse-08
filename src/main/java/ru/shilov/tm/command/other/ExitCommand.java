package ru.shilov.tm.command.other;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;

public final class ExitCommand extends AbstractTerminalCommand {

    @Override
    public @NotNull String getName() {
        return "exit";
    }

    @Override
    public @NotNull String getDescription() {
        return "Завершение работы";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Collections.emptyList();
    }

}
