package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class ProjectRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        @NotNull final String projectId = serviceLocator.getProjectService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        serviceLocator.getProjectService().removeOneByUserId(projectId, userId);
        System.out.println("[ПРОЕКТ УДАЛЕН]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public @NotNull String getName() {
        return "project-remove";
    }

    @Override
    public @NotNull String getDescription() {
        return "Удаление проекта";
    }

}
