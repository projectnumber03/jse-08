package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class ProjectFindOneCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ПРОЕКТА:");
        final String projectId = serviceLocator.getProjectService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        System.out.println(getProjectWithTasks(projectId, userId));
        System.out.println("[OK]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public @NotNull String getName() {
        return "project-tasks";
    }

    @Override
    public @NotNull String getDescription() {
        return "Список задач в проекте";
    }

    private @NotNull String getProjectWithTasks(@NotNull final String id, @NotNull final String userId) throws NoSuchEntityException {
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findByUserId(userId).stream()
                .filter(t -> id.equals(t.getProjectId()))
                .collect(Collectors.toList());
        return serviceLocator.getProjectService().findOne(id).getName() + ("\n")
                .concat(IntStream.range(1, tasks.size() + 1).boxed()
                        .map(i -> String.format("\t%d. %s", i, tasks.get(i - 1).getName()))
                        .collect(Collectors.joining("\n")));
    }

}
