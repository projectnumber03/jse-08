package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class ProjectRemoveAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        serviceLocator.getProjectService().removeAll();
        System.out.println("[ВСЕ ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @Override
    public @NotNull String getName() {
        return "project-remove-all";
    }

    @Override
    public @NotNull String getDescription() {
        return "Полное удаление всех проектов";
    }

}
