package ru.shilov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityRemoveException;

import java.util.Arrays;
import java.util.List;

public final class ProjectClearCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws EntityRemoveException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        serviceLocator.getProjectService().removeByUserId(userId);
        System.out.println("[ПРОЕКТЫ УДАЛЕНЫ]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public @NotNull String getName() {
        return "project-clear";
    }

    @Override
    public @NotNull String getDescription() {
        return "Удаление всех проектов";
    }

}
