package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public final class UserLogoutCommand extends AbstractTerminalCommand {

    @Override
    public void execute() {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        serviceLocator.getAuthorizationService().setCurrentUser(Optional.empty());
        System.out.println("[OK]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Collections.emptyList();
    }

    @Override
    public @NotNull String getName() {
        return "logout";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выход из системы";
    }

}
