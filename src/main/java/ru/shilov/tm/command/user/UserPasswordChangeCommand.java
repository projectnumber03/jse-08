package ru.shilov.tm.command.user;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserPasswordChangeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final User u = serviceLocator.getUserService().findOne(serviceLocator.getAuthorizationService().getCurrentUserId());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(DigestUtils.md5Hex(serviceLocator.getTerminalService().nextLine()));
        serviceLocator.getUserService().merge(u);
        System.out.println("[OK]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @Override
    public @NotNull String getName() {
        return "password-change";
    }

    @Override
    public @NotNull String getDescription() {
        return "Изменение пароля";
    }

}
