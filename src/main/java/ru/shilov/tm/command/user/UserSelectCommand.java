package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class UserSelectCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        System.out.println("ВВЕДИТЕ ID ПОЛЬЗОВАТЕЛЯ:");
        @NotNull final String userId = serviceLocator.getUserService().getId(serviceLocator.getTerminalService().nextLine());
        @NotNull final User u = serviceLocator.getUserService().findOne(userId);
        System.out.println(u.toString());
        System.out.println("[OK]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN);
    }

    @Override
    public @NotNull String getName() {
        return "user-select";
    }

    @Override
    public @NotNull String getDescription() {
        return "Свойства пользователя";
    }

}
