package ru.shilov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Collections;
import java.util.List;

public final class UserRegisterCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        @NotNull final User u = new User();
        System.out.println("ВВЕДИТЕ ЛОГИН:");
        u.setLogin(serviceLocator.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ПАРОЛЬ:");
        u.setPassword(serviceLocator.getTerminalService().nextLine());
        u.setRole(User.Role.USER);
        serviceLocator.getUserService().persist(u);
        System.out.println("[OK]");
    }

    @Override
    public @NotNull List<User.Role> getRoles() {
        return Collections.emptyList();
    }

    @Override
    public @NotNull String getName() {
        return "register";
    }

    @Override
    public @NotNull String getDescription() {
        return "Регистрация пользователя";
    }

}
