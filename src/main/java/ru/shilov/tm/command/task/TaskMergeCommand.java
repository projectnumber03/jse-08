package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

public final class TaskMergeCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        final String taskId = serviceLocator.getTaskService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        final Task t = serviceLocator.getTaskService().findOne(taskId);
        System.out.println("ВВЕДИТЕ НАЗВАНИЕ ЗАДАЧИ:");
        t.setName(serviceLocator.getTerminalService().nextLine());
        System.out.println("ВВЕДИТЕ ОПИСАНИЕ:");
        t.setDescription(serviceLocator.getTerminalService().nextLine());
        try {
            System.out.println("ВВЕДИТЕ ДАТУ НАЧАЛА:");
            t.setStart(LocalDate.parse(serviceLocator.getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
            System.out.println("ВВЕДИТЕ ДАТУ ОКОНЧАНИЯ:");
            t.setFinish(LocalDate.parse(serviceLocator.getTerminalService().nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        } catch (DateTimeParseException e) {
            throw new ru.shilov.tm.error.DateTimeParseException();
        }
        serviceLocator.getTaskService().merge(t);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Редактирование задачи";
    }

}
