package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.Task;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.NoSuchEntityException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class TaskFindAllCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws NoSuchEntityException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println(getAllTasks(userId));
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Список задач";
    }

    private String getAllTasks(final String userId) throws NoSuchEntityException {
        final List<Task> tasks = serviceLocator.getTaskService().findByUserId(userId);
        return IntStream.range(1, tasks.size() + 1).boxed()
                .map(i -> (String.format("%d. %s", i, tasks.get(i - 1).getName())))
                .collect(Collectors.joining("\n"));
    }

}
