package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;
import ru.shilov.tm.error.EntityRemoveException;

import java.util.Arrays;
import java.util.List;

public final class TaskClearCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws EntityRemoveException {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        serviceLocator.getTaskService().removeByUserId(userId);
        System.out.println("[ЗАДАЧИ УДАЛЕНЫ]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление всех задач";
    }

}
