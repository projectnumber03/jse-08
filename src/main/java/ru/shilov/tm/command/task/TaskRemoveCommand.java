package ru.shilov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.command.AbstractTerminalCommand;
import ru.shilov.tm.entity.User;

import java.util.Arrays;
import java.util.List;

public final class TaskRemoveCommand extends AbstractTerminalCommand {

    @Override
    public void execute() throws Exception {
        System.out.println(String.format("[%s]", this.getDescription().toUpperCase()));
        final String userId = serviceLocator.getAuthorizationService().getCurrentUserId();
        System.out.println("ВВЕДИТЕ ID ЗАДАЧИ:");
        final String taskId = serviceLocator.getTaskService().getId(serviceLocator.getTerminalService().nextLine(), userId);
        serviceLocator.getTaskService().removeOneByUserId(taskId, userId);
        System.out.println("[ЗАДАЧА УДАЛЕНА]");
    }

    @NotNull
    @Override
    public List<User.Role> getRoles() {
        return Arrays.asList(User.Role.ADMIN, User.Role.USER);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление задачи";
    }

}
