package ru.shilov.tm.command;

import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.context.ServiceLocator;
import ru.shilov.tm.entity.User;

import java.util.List;

public abstract class AbstractTerminalCommand {

    @Setter
    protected ServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    public abstract @NotNull List<User.Role> getRoles();

    public abstract @NotNull String getName();

    public abstract @NotNull String getDescription();

}
