package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IUserRepository;
import ru.shilov.tm.entity.User;

import java.util.*;

public final class UserRepositoryImpl extends AbstractRepository<User> implements IUserRepository {

    @Override
    public @NotNull Boolean removeOneByUserId(@NotNull final String id, @NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && !entry.getValue().getId().equals(userId));
    }

    @Override
    public @NotNull Optional<User> findByLoginPassword(@NotNull final String login, @NotNull final String passwd) {
        return entities.values().stream().filter(u -> login.equals(u.getLogin()) && passwd.equals(u.getPassword())).findAny();
    }

    @Override
    public @NotNull Boolean containsLogin(@NotNull final String login) {
        return entities.values().stream().anyMatch(u -> login.equals(u.getLogin()));
    }

    @Override
    public @NotNull String getId(@NotNull final String value) {
        return entities.size() >= Integer.parseInt(value) ? new ArrayList<>(entities.values()).get(Integer.parseInt(value) - 1).getId() : "";
    }

}
