package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shilov.tm.api.repository.IRepository;
import ru.shilov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    protected @NotNull final Map<String, T> entities = new LinkedHashMap<>();

    @Override
    public @NotNull List<T> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public @Nullable T findOne(@NotNull final String id) {
        return entities.values().stream().filter(entity -> entity.getId().equals(id)).findAny().orElse(null);
    }

    @Override
    public @NotNull List<T> findByUserId(@NotNull final String userId) {
        return entities.values().stream().filter(entity -> entity.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public @NotNull Boolean removeByUserId(@NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getUserId().equals(userId));
    }

    @Override
    public @NotNull Boolean removeOneByUserId(@NotNull final String id, @NotNull final String userId) {
        return entities.entrySet().removeIf(entry -> entry.getValue().getId().equals(id) && entry.getValue().getUserId().equals(userId));
    }

    @Override
    public @Nullable T persist(@NotNull final T entity) {
        return entities.putIfAbsent(entity.getId(), entity);
    }

    @Override
    public @NotNull T merge(@NotNull final T entity) {
        return entities.merge(entity.getId(), entity, (oldEntity, newEntity) -> newEntity);
    }

}
