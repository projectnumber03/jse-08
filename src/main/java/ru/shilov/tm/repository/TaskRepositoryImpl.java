package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.ITaskRepository;
import ru.shilov.tm.entity.Task;

import java.util.*;

public final class TaskRepositoryImpl extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public @NotNull String getId(@NotNull final String value, @NotNull final String userId) {
        @NotNull final List<Task> entities = findByUserId(userId);
        return entities.size() >= Integer.parseInt(value) ? entities.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
