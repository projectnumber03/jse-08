package ru.shilov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.shilov.tm.api.repository.IProjectRepository;
import ru.shilov.tm.entity.Project;

import java.util.*;

public final class ProjectRepositoryImpl extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public @NotNull String getId(@NotNull final String value, @NotNull final String userId) {
        final List<Project> projects = findByUserId(userId);
        return projects.size() >= Integer.parseInt(value) ? projects.get(Integer.parseInt(value) - 1).getId() : "";
    }

}
