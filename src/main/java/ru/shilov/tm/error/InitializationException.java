package ru.shilov.tm.error;

public final class InitializationException extends Exception {

    public InitializationException() {
        super("Ошибка инициализации");
    }

}
