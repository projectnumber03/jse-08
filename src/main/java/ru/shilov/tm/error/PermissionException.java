package ru.shilov.tm.error;

public final class PermissionException extends Exception {

    public PermissionException() {
        super("Недостаточно привилегий");
    }

}
