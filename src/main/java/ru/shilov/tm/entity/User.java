package ru.shilov.tm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class User extends AbstractEntity {

    private @Nullable String login;

    private @Nullable String password;

    private @Nullable Role role;

    @Override
    public @NotNull String getUserId() {
        return super.id;
    }

    @Override
    public @NotNull String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Пользователь: ").append(login).append("\n");
        sb.append("Роль: ").append(role != null && role.description != null ? role.description : "n/a");
        return sb.toString();
    }

    @AllArgsConstructor
    @Getter
    public enum Role {

        USER("Пользователь"),
        ADMIN("Администратор");

        private final String description;

    }

}
