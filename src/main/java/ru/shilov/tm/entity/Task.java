package ru.shilov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    private @Nullable String name;

    private @Nullable String description;

    private @Nullable LocalDate start;

    private @Nullable LocalDate finish;

    private @Nullable String projectId;

    private @Nullable String userId;

    @Override
    public @NotNull String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("Задача: ").append(this.name).append("\n");
        sb.append("Описание: ").append(this.description).append("\n");
        sb.append("Дата начала: ").append(start != null ? DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.start) : "n/a").append("\n");
        sb.append("Дата окончания: ").append(finish != null ? DateTimeFormatter.ofPattern("dd.MM.yyyy").format(this.finish) : "n/a");
        return sb.toString();
    }

}
